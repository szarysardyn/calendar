#include <stdio.h>
#include <string.h>
#include <math.h>

int day, newmonth, year, dt, firstday;
int days_in_month[]={0,31,28,31,30,31,30,31,31,30,31,30,31};
char *months[] = {" ", "styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "październik", "listopad", "grudzień"};
char *months1[] = {" ", "stycznia", "lutego", "marca", "kwietnia", "maja", "czerwca", "lipca", "sierpnia", "września", "października", "listopada", "grudnia"};
char month[20];


//sprawdź czy rok jest przestępny
int leapyear()
{
	if(year%4==0 && (year%100!=0 || year%400==0))
	{
		days_in_month[2] = 29;
	}
	else
	{
		days_in_month[2] = 28;
	}
	return 0;
}

//wprowadź datę i ustal numer miesiąca
int date()
{
    int i;
	printf("Podaj datę (np. 26 marzec 1943): ");
    scanf("%d %20s %d", &day, month, &year);
	for(i=1; i<=12; i++) if(strcmp(month, months[i])==0 || strcmp(month, months1[i])==0) newmonth=i;
    //dodaj 0 dla numerów dni i miesięcy mniejszych od 10
    if(newmonth<10 && day<10) printf("Podałeś datę %d %s %d, czyli 0%d.0%d.%d\n", day, month, year, day, newmonth, year);
	else if(newmonth<10) printf("Podałeś datę %d %s %d, czyli %d.0%d.%d\n", day, month, year, day, newmonth, year);
	else if(day<10) printf("Podałeś datę %d %s %d, czyli 0%d.%d.%d\n", day, month, year, day, newmonth, year);
	else printf("Podałeś datę %d %s %d, czyli %d.%d.%d\n", day, month, year, day, newmonth, year);
	return 0;
}

//ustal dzień tygodnia
int determineday()
{
    int m, k, j, y, dt1;
    if(newmonth==1) m = 13, y = year - 1;
    else if(newmonth==2) m = 14, y = year - 1;
	else m = newmonth, y = year;
    k = y%100;
    j = floor(y/100);
    dt1 = (day + (int)floor(13*(m + 1)/5) + k + (int)floor(k/4) + (int)floor(j/4) + 5*j)%7;
    dt = (dt1 + 5)%7 + 1;
    if(dt == 1) printf("To był poniedziałek.\n");
    else if(dt == 2) printf("To był wtorek.\n");
    else if(dt == 3) printf("To była środa.\n");
    else if(dt == 4) printf("To był czwartek.\n");
    else if(dt == 5) printf("To był piątek.\n");
    else if(dt == 6) printf("To była sobota.\n");
    else if(dt == 7) printf("To była niedziela.\n");
	return 0;
}
// oblicz dzień juliański
int julianday()
{
    int JD;
    float d, m, y, a, b, c, e, f, g, h;
    m = newmonth;
    y = year;
    d = day;
    a = (m + 9) / 12;
    b = 4716 + y + floor(a);
    c = 275 * m / 9;
    e = 7 * b / 4;
    f = 1729279 + 367 * y + floor(c) - floor(e) + d;
    g = floor((b + 83) / 100);
    h = floor(3 * (g + 1) / 4);
    JD = f + 38 - h;
    printf ("To był %d dzień juliański.\n\n", JD);
	return 0;
}
// ustal dzień tygodnia początku wprowadzonego roku
int determinefirstday()
{
  int y, k, j, x;
    y=year-1;
	k = y%100;
    j = floor(y/100);
    x = (1 + (int)floor(13*(13 + 1)/5) + k + (int)floor(k/4) + (int)floor(j/4) + 5*j)%7;
    firstday = (x + 5)%7;
	return 0;
}
void calendar()
{
	int i, j;
	for (i=1; i<=12; i++)
	{
		printf("\n             %s\n", months[i]);
		printf("-----------------------------------\n");
		printf(" pn | wt | śr | cz | pt | sb | nd |\n" );
		printf("-----------------------------------\n");
		// poprawka na początek miesiąca
		for (j=1; j<=firstday; j++) printf("  * |");

		// wyświetl wszystkie daty dla obecnego miesiąca
		for (j=1; j<=days_in_month[i]; j++)
		{
            // zaznacz wprowadzoną datę w kalendarzu
			if(j==1 && i==newmonth && j==day) printf(" %2d*|", j);
            else if(i==newmonth && j==day) printf("%2d*|", j);
			else if(j==1) printf(" %2d |", j);
            else printf("%2d |", j);

			// uwzględnienie tygodni
			if((j + firstday)%7 > 0) printf(" ");
			else printf("\n ");
		}
		// ustal początek następnego miesiąca
		firstday = (firstday + days_in_month[i]) % 7;
	printf("\n");
	}
}

int main ()
{
	date();
	leapyear();
	determineday();
	julianday();
    determinefirstday();
    calendar();
	return 0;
}
